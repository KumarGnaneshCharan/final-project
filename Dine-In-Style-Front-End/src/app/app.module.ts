import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegisterPart2Component } from './register-part2/register-part2.component';
import { OtpValidateComponent } from './otp-validate/otp-validate.component';
import { EncrDecrService } from './encr-decr.service';
import { RegisterotpComponent } from './registerotp/registerotp.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginbyemailComponent } from './loginbyemail/loginbyemail.component';
import { Header1Component } from './header1/header1.component';
import { HomepageBodyComponent } from './homepage-body/homepage-body.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RestaurantBookingPageComponent } from './restaurant-booking-page/restaurant-booking-page.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { ReviewPipe } from './review.pipe';
import { ProfileComponent } from './profile/profile.component';
import { UserComponent } from './user/user.component';
import { HotelReservationsComponent } from './hotel-reservations/hotel-reservations.component';
import { RestaurantReservationsComponent } from './restaurant-reservations/restaurant-reservations.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetOtpComponent } from './reset-otp/reset-otp.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { LogoutComponent } from './logout/logout.component';
import { UserapiService } from './userapi.service';
import { UserService } from './user.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    LandingpageComponent,
    LoginComponent,
    RegisterComponent,
    RegisterPart2Component,
    OtpValidateComponent,
    RegisterotpComponent,
    HomepageComponent,
    RestaurantComponent,
    LoginbyemailComponent,
    Header1Component,
    HomepageBodyComponent,
    ContactUsComponent,
    AboutUsComponent,
    RestaurantBookingPageComponent,
    HotelComponent,
    HotelBookingComponent,
    ReviewPipe,
    ProfileComponent,
    UserComponent,
    HotelReservationsComponent,
    RestaurantReservationsComponent,
    ForgotPasswordComponent,
    ResetOtpComponent,
    PasswordResetComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NgxCaptchaModule
  ],
  providers: [EncrDecrService,UserapiService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
