import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-booking',
  templateUrl: './hotel-booking.component.html',
  styleUrls: ['./hotel-booking.component.css']
})
export class HotelBookingComponent implements OnInit {
  facilties:any[];
  hotel:any;
  EmailBody:any;

  constructor(private router:Router){
    this.facilties=[
      {img:'assets/images/breakfast.png',name:'Breakfast'},
      {img:'assets/images/private bathroom.png',name:'Private Bathroom'},
      {img:'assets/images/free parking.png',name:'Free Parkign'},
      {img:'assets/images/shuttle service.png',name:'Shuttle Service'},
      {img:'assets/images/pets allowed.png',name:'Pets allowed'},
      {img:'assets/images/balcony.png',name:'Balcony'},
      {img:'assets/images/view.png',name:'View'},
      {img:'assets/images/free wifi.png',name:'Free WiFi'},
      {img:'assets/images/family room.png',name:'Family rooms'},
      {img:'assets/images/ac.png',name:'Air conditioning'},
    ];
    this.EmailBody={
      toEmail:"",
      subject:"",
      body:""
    };
    this.hotel={
      img1: "",
      img2: "",
      img3: "",
      name: "",
      stars: "",
      location:"",
      maplink: "",
      distance: "",
      reviwers: "",
      rating: "",
      rate: "",
      disrate: "",
      tax: ""
    };
  }

  ngOnInit() {
    const storedHotel=localStorage.getItem("hotel");
    if (storedHotel !== null) {
      this.hotel = JSON.parse(storedHotel);
    } else {
      console.error("Hotel not found in local storage");
    }
  }

  check(){
    alert("Thanks for booking your table in "+this.hotel.name+".We will reach you shortly by email. Have a nice day");
    this.router.navigate(['/homepage']);
  }
}
