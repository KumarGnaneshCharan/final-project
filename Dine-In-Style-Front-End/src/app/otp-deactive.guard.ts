import { inject } from '@angular/core';
import { CanDeactivateFn } from '@angular/router';
import { UserService } from './user.service';

export const otpDeactiveGuard: CanDeactivateFn<unknown> = (component, currentRoute, currentState, nextState) => {
  let service=inject(UserService);
  return service.getCanGo();
};
