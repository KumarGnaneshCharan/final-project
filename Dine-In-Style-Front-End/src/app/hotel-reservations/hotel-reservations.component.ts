import { Component } from '@angular/core';

@Component({
  selector: 'app-hotel-reservations',
  templateUrl: './hotel-reservations.component.html',
  styleUrls: ['./hotel-reservations.component.css']
})
export class HotelReservationsComponent {
  hotels: any;
  constructor() {
    this.hotels = [
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/21/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/16/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/09/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/17/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/22/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/26/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/22/2023', Days: '2' },
      { hotel: 'Hotel Peral City', Suiteroom: 'Luxury', Date: '11/22/2023', Days: '2' },
    ];    
  }
}
