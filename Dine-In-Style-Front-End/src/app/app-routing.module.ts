import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterPart2Component } from './register-part2/register-part2.component';
import { OtpValidateComponent } from './otp-validate/otp-validate.component';
import { RegisterotpComponent } from './registerotp/registerotp.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginbyemailComponent } from './loginbyemail/loginbyemail.component';
import { otpAuthGuard } from './otp-auth.guard';
import { otpDeactiveGuard } from './otp-deactive.guard';
import { register2Guard } from './register2.guard';
import { registerDeactivateGuard } from './register-deactivate.guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetOtpComponent } from './reset-otp/reset-otp.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { HomepageBodyComponent } from './homepage-body/homepage-body.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ProfileComponent } from './profile/profile.component';
import { RestaurantBookingPageComponent } from './restaurant-booking-page/restaurant-booking-page.component';
import { UserComponent } from './user/user.component';
import { HotelReservationsComponent } from './hotel-reservations/hotel-reservations.component';
import { RestaurantReservationsComponent } from './restaurant-reservations/restaurant-reservations.component';
import { LogoutComponent } from './logout/logout.component';
import { loginAuthGuard } from './login-auth.guard';

const routes: Routes = [
  { path: "", component: LandingpageComponent },
  {
    path: "login", children: [
      { path: '', component: LoginComponent },
      { path: "otpValidate", canActivateChild: [otpAuthGuard], canDeactivate: [otpDeactiveGuard], component: OtpValidateComponent },
      {
        path: 'registerotp', children: [
          { path: '', canActivateChild: [otpAuthGuard], canDeactivate: [otpDeactiveGuard], component: RegisterotpComponent },
          { path: 'register2', canActivate: [register2Guard], canDeactivate: [registerDeactivateGuard], component: RegisterPart2Component }
        ]
      }
    ]
  },
  {
    path: 'register', children: [
      { path: '', component: RegisterComponent },
      { path: "otpValidate", canActivateChild: [otpAuthGuard], canDeactivate: [otpDeactiveGuard], component: OtpValidateComponent },
      {
        path: 'registerotp', children: [
          { path: '', canActivateChild: [otpAuthGuard], canDeactivate: [otpDeactiveGuard], component: RegisterotpComponent },
          { path: 'register2', canActivate: [register2Guard], canDeactivate: [registerDeactivateGuard], component: RegisterPart2Component }
        ]
      }
    ]
  },
  {
    path: "loginwithgmail", children: [
      { path: '', component: LoginbyemailComponent },
      {
        path: 'forgotPassword', children: [
          { path: '', component: ForgotPasswordComponent },
          {
            path: 'resetOtp', children: [
              { path: '', canActivateChild: [otpAuthGuard], canDeactivate: [otpDeactiveGuard], component: ResetOtpComponent },
              { path: 'passwordReset', canActivate: [register2Guard], canDeactivate: [registerDeactivateGuard], component: PasswordResetComponent }
            ]
          }
        ]
      }
    ]
  },
  {
    path: "homepage", canActivate: [loginAuthGuard], component: HomepageComponent, children: [
      { path: '', component: HomepageBodyComponent },
      { path: 'aboutus', component: AboutUsComponent },
      {
        path: 'profile', component: ProfileComponent, children: [
          { path: '', component: UserComponent },
          { path: 'user', component: UserComponent },
          { path: 'hotelreservations', component: HotelReservationsComponent },
          { path: 'restaurantreservations', component: RestaurantReservationsComponent }
        ]
      },
      { path: 'contactus', component: ContactUsComponent },
      { path: 'restaurentlist', component: RestaurantComponent },
      { path: 'hotellist', component: HotelComponent },
      { path: 'hotelbooking', component: HotelBookingComponent },
      { path: "logout", component: LogoutComponent }
    ]
  },
  { path: "restaurantbooking", canActivate: [loginAuthGuard], component: RestaurantBookingPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
