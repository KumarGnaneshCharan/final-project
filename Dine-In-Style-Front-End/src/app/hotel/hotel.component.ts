import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent {
  hotels:any;

  constructor(private router:Router, private route: ActivatedRoute){
    this.hotels=[
      {
        img1: "assets/hotels/Hotel lake view airport zone/room1.jpg",
        img2: "assets/hotels/Hotel lake view airport zone/room2.jpg",
        img3: "assets/hotels/Hotel lake view airport zone/room3.jpg",
        name: "Hotel Lake View Airport zone",
        stars: "★★★",
        location: "11-3/1/B mettas Building Airport Road Beside HDFC BANK Madhura Nagar Shamshabad Hyderabad Telangana, 501218 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/DpHnW3MFQ17KRZL5A",
        distance: "1.7km",
        reviwers: "127",
        rating: "7.1",
        rate: "₹3,885",
        disrate: "2,331",
        tax: "280"
      },
      {
        img1: "assets/hotels/westin hydearabad mindspace/room1.jpg",
        img2: "assets/hotels/westin hydearabad mindspace/room2.jpg",
        img3: "assets/hotels/westin hydearabad mindspace/room3.jpg",
        name: "The Westin Hyderabad Mindspace",
        stars: "★★★★",
        location: "Raheja It Park Hitec City, Madhapur, 500081 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/tP1Z2TUnm9FgMTum6",
        distance: "6.3km",
        reviwers: "23",
        rating: "8.8",
        rate: "",
        disrate: "3,600",
        tax: "432"
      },
      {
        img1: "assets/hotels/OYO Flagship/room1.jpg",
        img2: "assets/hotels/OYO Flagship/room2.jpg",
        img3: "assets/hotels/OYO Flagship/room3.jpg",
        name: "OYO Flagship Hotel Srh Pride",
        stars: "★★★",
        location: "MIG149 KPHB Phase 15 near Lodha green hills road, Hyderabad, 500072 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/e3Cpy7h9N8gtKUDs9",
        distance: "7.5km",
        reviwers: "8",
        rating: "7.8",
        rate: "₹2,667",
        disrate: "1,120",
        tax: "151"
      },
      {
        img1: "assets/hotels/fab hotel rooms/room1.jpg",
        img2: "assets/hotels/fab hotel rooms/room2.jpg",
        img3: "assets/hotels/fab hotel rooms/room3.jpg",
        name: "FabHotel Rooms",
        stars: "★★★",
        location: "Plot no 469, Road No: 26, Swamy Ayyappa Society, Madhapur, 500081 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/VGmsuqEwqm9dMjRk7",
        distance: "6.5km",
        reviwers: "132",
        rating: "8.5",
        rate: "₹2,750",
        disrate: "1,705",
        tax: "205"
      },
      {
        img1: "assets/hotels/the golconda hotel/room1.jpg",
        img2: "assets/hotels/the golconda hotel/room2.jpg",
        img3: "assets/hotels/the golconda hotel/room3.jpg",
        name: "The Golkonda Hotel",
        stars: "★★★★",
        location: "10-1-124, Mehdipatnam - Banjara Hills Rd, Ambedkar Nagar, Masab Tank, 500028 Hyderabad, India ",
        maplink: "https://maps.app.goo.gl/KedGStwm5UwzBnAE7",
        distance: "2.1km",
        reviwers: "635",
        rating: "7.5",
        rate: "₹5,625",
        disrate: "4,500",
        tax: "540"
      },
      {
        img1: "assets/hotels/casa hotel and suites/room1.jpg",
        img2: "assets/hotels/casa hotel and suites/room2.jpg",
        img3: "assets/hotels/casa hotel and suites/room3.jpg",
        name: "Casa Hotel & Suites, Gachibowli, Hyderabad",
        stars: "★★★★",
        location: "Plot no 7,Opp US Consulate Nanakramguda, Dist, Financial District, Vattinagulapally, Hyderabad, Telangana 500075, 500075 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/qB5DG4pMuXV3PyEx6",
        distance: "11.6km",
        reviwers: "27",
        rating: "8.8",
        rate: "₹4,800",
        disrate: "2,400",
        tax: "288"
      },
      {
        img1: "assets/hotels/treebo trend/room1.jpg",
        img2: "assets/hotels/treebo trend/room2.jpg",
        img3: "assets/hotels/treebo trend/room3.jpg",
        name: "Treebo Trend The Rise",
        stars: "★★★",
        location: "Plot No 188, Swamy Ayyappa Society, Khanamet, Madhapur, 500081 Hyderabad, India ",
        maplink: "https://maps.app.goo.gl/UcyvD3x214Urpgkw9",
        distance: "6.2km",
        reviwers: "14",
        rating: "8.9",
        rate: "₹3,555",
        disrate: "2,026",
        tax: "243"
      },
      {
        img1: "assets/hotels/hotel Axis/room1.jpg",
        img2: "assets/hotels/hotel Axis/room2.jpg",
        img3: "assets/hotels/hotel Axis/room3.jpg",
        name: "Hotel Axis Holidays - Banjara Hills",
        stars: "★★★",
        location: "Lane Number 12, 500034 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/KH4i7qnJbKY4Jirq9",
        distance: "1.7km",
        reviwers: "17",
        rating: "8.0",
        rate: "₹3,060",
        disrate: "1,530",
        tax: "184"
      },
      {
        img1: "assets/hotels/Taj deccan/room1.jpg",
        img2: "assets/hotels/Taj deccan/room2.jpg",
        img3: "assets/hotels/Taj deccan/room3.jpg",
        name: "Taj Deccan",
        stars: "★★★★★",
        location: "Banjara Hills Main Rd, Balapur Basthi, Banjara Hills, 500034 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/auaMyND37ZkzYPrq6",
        distance: "1.3km",
        reviwers: "1,134",
        rating: "8.1",
        rate: "",
        disrate: "5,490",
        tax: "659"
      },
      {
        img1: "assets/hotels/hyatt/room1.jpg",
        img2: "assets/hotels/hyatt/room2.jpg",
        img3: "assets/hotels/hyatt/room3.jpg",
        name: "Hyatt Place Hyderabad Banjara Hills",
        stars: "★★★★★",
        location: "Road 1, Banjara Hills, 500034 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/HM3zEyD3V2i2AzKp8",
        distance: "1km",
        reviwers: "₹1,293",
        rating: "7.9",
        rate: "",
        disrate: "5,999",
        tax: "1,080"
      },
      {
        img1: "assets/hotels/western suites/room1.jpeg",
        img2: "assets/hotels/western suites/room2.jpeg",
        img3: "assets/hotels/western suites/room3.jpeg",
        name: "Best Western Ashoka",
        stars: "★★★",
        location: "Plot No. 597, Swamy Ayyappa Society, VIP Hills, Silicon Valley, Madhapur, 500081 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/HM3zEyD3V2i2AzKp8",
        distance: "3.2km",
        reviwers: "498",
        rating: "7.3",
        rate: "",
        disrate: "2,790",
        tax: "335"
      }
    ]

    
  }

  availabilty(hotel:any){
    localStorage.setItem("hotel",JSON.stringify(hotel));
    this.router.navigate(['homepage/hotelbooking']);
  }
}
