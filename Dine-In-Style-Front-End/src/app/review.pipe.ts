import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'review'
})
export class ReviewPipe implements PipeTransform {

  transform(rating:any): any {
    if(rating>8.5) return 'Fabulous';
    else if (rating >7.8 && rating <=8.5 )return 'Very Good';
    else return 'Good';
  }

}
