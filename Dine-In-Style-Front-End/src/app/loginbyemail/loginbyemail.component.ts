import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EncrDecrService } from '../encr-decr.service';
import { UserapiService } from '../userapi.service';
import { enc } from 'crypto-js';
import { UserService } from '../user.service';

@Component({
  selector: 'app-loginbyemail',
  templateUrl: './loginbyemail.component.html',
  styleUrls: ['./loginbyemail.component.css']
})
export class LoginbyemailComponent implements OnInit {
  isRightPanelActive = false;
  users: any;
  siteKey: string = '6LcgXQspAAAAALuQi1v4_7ArGsrz_ghg6lN0Inv_';
  captchaResponse: String = '';

  constructor(private router: Router, private service: EncrDecrService, private apiservice: UserapiService, private service1: UserService) {

  }

  ngOnInit() {
    this.apiservice.getAllUsers().subscribe((data: any) => {
      this.users = data;
    })
  }
  onMobileInput(event: Event) {
    const input = event.target as HTMLInputElement;
    let inputValue = input.value?.toString() || '';
    if (inputValue.length > 10) {
      input.value = inputValue.slice(0, 10);
    } else {
      input.value = inputValue;
    }
  }

  login(loginbyemailForm: any) {
    let userFound = false;

    for (let user of this.users) {
      if (user.emailId === loginbyemailForm.emailId && user.password === loginbyemailForm.password) {
        userFound = true;
        localStorage.setItem("mainemail", loginbyemailForm.emailId);
        this.service1.setUserLogin();
        this.router.navigate(['homepage']);
        break;
      }
    }

    if (!userFound) {
      alert("Wrong emailId/password");
    }
  }
}


