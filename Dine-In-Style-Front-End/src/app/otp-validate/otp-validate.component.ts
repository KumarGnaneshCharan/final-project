import { AfterViewInit, Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { UserapiService } from '../userapi.service';

@Component({
  selector: 'app-otp-validate',
  templateUrl: './otp-validate.component.html',
  styleUrls: ['./otp-validate.component.css']
})
export class OtpValidateComponent implements AfterViewInit {
  userOtp:any;
  cango:any;
  @ViewChild('input1') input1!: ElementRef;
  @ViewChild('input2') input2!: ElementRef;
  @ViewChild('input3') input3!: ElementRef;
  @ViewChild('input4') input4!: ElementRef;
  @ViewChild('button') button!: ElementRef;

  constructor(private router:Router, private service: UserService,private apiservice:UserapiService) {
    this.userOtp={
      "phoneNumber":"",
      "otpNumber":""
    }
  }

  readInputValues() {
    const input1Value = this.input1.nativeElement.value;
    const input2Value = this.input2.nativeElement.value;
    const input3Value = this.input3.nativeElement.value;
    const input4Value = this.input4.nativeElement.value;
    var otp=""+input1Value+input2Value+input3Value+input4Value;
    this.userOtp.phoneNumber=localStorage.getItem("phno");
    this.userOtp.otpNumber=otp;
    this.apiservice.sendOtp(this.userOtp).subscribe((data:any)=>{
      if(data!=null){
        this.service.setUserLogin();
        this.service.setCanGo();
        this.router.navigate(['homepage']);
        this.service.unsetotp();
      } else{
        alert("Invalid OTP")
      }
    });
  }

  ngAfterViewInit() {
    this.input1.nativeElement.focus();
    const inputs = [this.input1, this.input2, this.input3, this.input4];
    inputs.forEach((input, index1) => {
      input.nativeElement.addEventListener("keyup", (e: KeyboardEvent) => {
        const currentInput = input.nativeElement;
        const nextInput = inputs[index1 + 1]?.nativeElement;
        const prevInput = inputs[index1 - 1]?.nativeElement;

        if (currentInput.value.length > 1) {
          currentInput.value = currentInput.value.charAt(0);
          return;
        }

        if (nextInput && nextInput.hasAttribute("disabled") && currentInput.value !== "") {
          nextInput.removeAttribute("disabled");
          nextInput.focus();
        }

        if (e.key === "Backspace") {
          inputs.forEach((input, index2) => {
            if (index1 <= index2 && prevInput) {
              input.nativeElement.setAttribute("disabled", "true");
              input.nativeElement.value = "";
              prevInput.focus();
            }
          });
        }

        if (!inputs[3].nativeElement.hasAttribute("disabled") && inputs[3].nativeElement.value !== "") {
          this.button.nativeElement.classList.add("active");
          return;
        } else{
          this.button.nativeElement.classList.remove("active");
        }
      });
    });
  }
}