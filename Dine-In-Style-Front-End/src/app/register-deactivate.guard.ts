import { inject } from '@angular/core';
import { CanDeactivateFn } from '@angular/router';
import { UserService } from './user.service';

export const registerDeactivateGuard: CanDeactivateFn<unknown> = (component, currentRoute, currentState, nextState) => {
  let service=inject(UserService);
  return service.getCanGo();
};
