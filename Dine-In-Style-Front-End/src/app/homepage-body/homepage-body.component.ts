import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-homepage-body',
  templateUrl: './homepage-body.component.html',
  styleUrls: ['./homepage-body.component.css']
})
export class HomepageBodyComponent implements OnInit {
  restaurants: any;
  hotels: any;
  constructor(private router: Router, private route: ActivatedRoute) {
    this.restaurants = [
      {img:"assets/restaurants/marsala/outlook.jpg",name:"Marsala",address:"Gachibwli",cost:"250",cusine:"Chicken ,Mutton Assorted Mandi",rating:"4.5",
        menu:"assets/restaurants/marsala/menu.jpg",ambience:"assets/restaurants/marsala/ambiencce.jpg",img1:"assets/restaurants/marsala/food.jpeg",
        img2:"assets/restaurants/marsala/food1.jpg",img3:"assets/restaurants/marsala/food2.jpg",img4:"assets/restaurants/marsala/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2274340613085!2d78.3537688737914!3d17.448825201035007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93257765bba7%3A0x8bc6ce2993732da6!2sMarsala%20Food%20Company%2C%20Gachibowli!5e0!3m2!1sen!2sin!4v1699722811555!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/kayal kerala/outlook.jpg",name:"Kayal kerala",address:"Gachibowli",cost:"250",cusine:"Fish thaali, Chicken thaali, Chatti choru",rating:"4.8",
        menu:"assets/restaurants/kayal kerala/menu.jpg",ambience:"assets/restaurants/kayal kerala/ambience.jpg",img1:"assets/restaurants/kayal kerala/food.jpg",
        img2:"assets/restaurants/kayal kerala/food1.png",img3:"assets/restaurants/kayal kerala/food2.jpg",img4:"assets/restaurants/kayal kerala/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2324140185224!2d78.35615917379143!3d17.448586701041926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9331e3d63643%3A0x4c998477a02f04fd!2sKayal%20Kerala%20Restaurant!5e0!3m2!1sen!2sin!4v1699721892949!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/mamagoto/outlook.jpg",name:"Mamagoto",address:"Western Pearl",cost:"400",cusine:"Pan-Asian foods",rating:"4.5",
        menu:"assets/restaurants/mamagoto/menu.jpg",ambience:"assets/restaurants/mamagoto/ambience.jpeg",img1:"assets/restaurants/mamagoto/food.jpg",
        img2:"assets/restaurants/mamagoto/food1.jpg",img3:"assets/restaurants/mamagoto/food2.jpg",img4:"assets/restaurants/mamagoto/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.02828615084!2d78.37050217379165!3d17.45836020075939!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93d026679481%3A0xa6647645dbdc4597!2sMamagoto!5e0!3m2!1sen!2sin!4v1699723291242!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/Nawaab's/outlook.jpg",name:"Nawaab's Restaurant",address:"Gachibowli",cost:"310",cusine:"Arabian Mandi, Khabsha",rating:"4.3",
        menu:"assets/restaurants/Nawaab's/menu.jpg",ambience:"assets/restaurants/Nawaab's/ambience.jpg",img1:"assets/restaurants/Nawaab's/food.jpg",
        img2:"assets/restaurants/Nawaab's/food1.jpg",img3:"assets/restaurants/Nawaab's/food2.jpg",img4:"assets/restaurants/Nawaab's/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2233331162047!2d78.36147277379143!3d17.449021601029482!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93ab7d8bcc7f%3A0xf8de83173cd01211!2sThe%20Nawaab&#39;s%20Restaurant!5e0!3m2!1sen!2sin!4v1699723327333!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/chitti gaare/outlook.jpg",name:"Kodi Kura Chitti Gara",address:"Gachibowli",cost:"200",cusine:"Raagi muddha, Natukodi pulusu",rating:"4.2",
        menu:"assets/restaurants/chitti gaare/menu.jpg",ambience:"assets/restaurants/chitti gaare/ambience.jpg",img1:"assets/restaurants/chitti gaare/food1.jpg",
        img2:"assets/restaurants/chitti gaare/food2.jpg",img3:"assets/restaurants/chitti gaare/food3.jpg",img4:"assets/restaurants/chitti gaare/food.jpeg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60897.55020340521!2d78.31917606108621!3d17.455076197411103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93c7b31eaaab%3A0xcece91941905d8f5!2sKodi%20Kura%20Chitti%20Gaare%2C%20Kondapur!5e0!3m2!1sen!2sin!4v1699721789667!5m2!1sen!2sin"
      }
    ];
    this.hotels = [
      {
        img1: "assets/hotels/treebo trend/room1.jpg",
        img2: "assets/hotels/treebo trend/room2.jpg",
        img3: "assets/hotels/treebo trend/room3.jpg",
        name: "Treebo Trend The Rise",
        stars: "★★★",
        location: "Plot No 188, Swamy Ayyappa Society, Khanamet, Madhapur, 500081 Hyderabad, India ",
        maplink: "https://maps.app.goo.gl/UcyvD3x214Urpgkw9",
        distance: "6.2km",
        reviwers: "14",
        rating: "8.9",
        rate: "₹3,555",
        disrate: "2,026",
        tax: "243"
      },
      {
        img1: "assets/hotels/westin hydearabad mindspace/room1.jpg",
        img2: "assets/hotels/westin hydearabad mindspace/room1.jpg",
        img3: "assets/hotels/westin hydearabad mindspace/room3.jpg",
        name: "The Westin Hyderabad Mindspace",
        stars: "★★★★",
        location: "Raheja It Park Hitec City, Madhapur, 500081 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/tP1Z2TUnm9FgMTum6",
        distance: "6.3km",
        reviwers: "23",
        rating: "8.8",
        rate: "",
        disrate: "3,600",
        tax: "432"
      },
      {
        img1: "assets/hotels/casa hotel and suites/room1.jpg",
        img2: "assets/hotels/casa hotel and suites/room2.jpg",
        img3: "assets/hotels/casa hotel and suites/room3.jpg",
        name: "Casa Hotel & Suites, Gachibowli, Hyderabad",
        stars: "★★★★",
        location: "Plot no 7,Opp US Consulate Nanakramguda, Dist, Financial District, Vattinagulapally, Hyderabad, Telangana 500075, 500075 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/qB5DG4pMuXV3PyEx6",
        distance: "11.6km",
        reviwers: "27",
        rating: "8.8",
        rate: "₹4,800",
        disrate: "2,400",
        tax: "288"
      },
      {
        img1: "assets/hotels/fab hotel rooms/room1.jpg",
        img2: "assets/hotels/fab hotel rooms/room2.jpg",
        img3: "assets/hotels/fab hotel rooms/room3.jpg",
        name: "FabHotel Rooms",
        stars: "★★★",
        location: "Plot no 469, Road No: 26, Swamy Ayyappa Society, Madhapur, 500081 Hyderabad, India",
        maplink: "https://maps.app.goo.gl/VGmsuqEwqm9dMjRk7",
        distance: "6.5km",
        reviwers: "132",
        rating: "8.5",
        rate: "₹2,750",
        disrate: "1,705",
        tax: "205"
      },
      {
        img1: "assets/hotels/the golconda hotel/room1.jpg",
        img2: "assets/hotels/the golconda hotel/room2.jpg",
        img3: "assets/hotels/the golconda hotel/room3.jpg",
        name: "The Golkonda Hotel",
        stars: "★★★★",
        location: "10-1-124, Mehdipatnam - Banjara Hills Rd, Ambedkar Nagar, Masab Tank, 500028 Hyderabad, India ",
        maplink: "https://maps.app.goo.gl/KedGStwm5UwzBnAE7",
        distance: "2.1km",
        reviwers: "635",
        rating: "7.5",
        rate: "₹5,625",
        disrate: "4,500",
        tax: "540"
      }
    ];    
  }
  ngOnInit() { }

  foodnavigate() {

    this.router.navigate(['restaurentlist'], { relativeTo: this.route });
  }

  hotelnavigate() {
    this.router.navigate(['hotellist'], { relativeTo: this.route });
  }

  showrestaurants(restaurant: any) {
    localStorage.setItem("restaurant",JSON.stringify(restaurant));
    this.router.navigate(['restaurantbooking']);
  }
  showhotels(hotel: any) {
    localStorage.setItem("hotel",JSON.stringify(hotel));
    this.router.navigate(['hotelbooking'], { relativeTo: this.route })
  }
}
