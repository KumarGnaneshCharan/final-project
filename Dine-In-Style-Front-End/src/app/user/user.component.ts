import { Component, OnInit } from '@angular/core';
import { UserapiService } from '../userapi.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isEditing = false;
  userData:any;
  constructor(private apiservice:UserapiService){
    this.userData={
      userId:0,
      userName:"",
      mobileNumber:0,
      emailId:"",
      password:""
    }
  }

  ngOnInit(){
    const phno=localStorage.getItem("mainph");
    const email=localStorage.getItem("mainemail");
    if(phno!=null){
      this.apiservice.getUserByNumber(phno).subscribe((data:any)=>{
        this.userData=data;
      });
    } else if(email!=null){
      this.apiservice.getuserByEmail(email).subscribe((data:any)=>{
        this.userData=data;
      });
    }
  }

  edit(){
    this.isEditing = true;
  }
  update(){
    this.apiservice.updateUser(this.userData).subscribe((data:any)=>{});
    this.isEditing = false;
  }
}
