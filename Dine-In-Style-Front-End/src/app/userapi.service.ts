import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserapiService {
  verifyOtp: any;

  constructor(private http:HttpClient) {}

  getAllUsers():any{
    return this.http.get('getAllUsers');
  }
  getUserByNumber(userNumber:any):any{
    return this.http.get('getUserByMobileNumber/'+userNumber);
  }
  getuserByEmail(emailId:any):any{
    return this.http.get('getUserByEmailId/'+emailId);
  }
  registerUser(user:any):any{
    return this.http.post('registerUser',user)
  }
  updateUser(userNumber:any):any{
    return this.http.put('updateUser',userNumber);
  }
  deleteUserbynumber(userNumber:any):any{
    return this.http.delete('deleteUserByNumber/'+userNumber);  
  }
  deleteUserbyemail(emailId:any):any{
    return this.http.delete('deleteUserByEmail/'+emailId);  
  }
  getOtp(userOtp:any):any{
    return this.http.post('otp/send',userOtp);
  }
  sendOtp(VerifyOtp:any):any{
    return this.http.post('otp/verify',VerifyOtp);
  }
  sendEmail(emailbody:any):any{
    return this.http.post('email/send',emailbody);
  }
  recieveEmail(emailbody:any):any{
    return this.http.post('email/recieve',emailbody);
  }
}