import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { UserapiService } from '../userapi.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isRightPanelActive = false;
  otpNumber: any;
  users: any;
  constructor(private router: Router, private route: ActivatedRoute, private service: UserService, private apiservice: UserapiService) {
    this.otpNumber = { "phoneNumber": "" };
  }

  ngOnInit() {
    this.apiservice.getAllUsers().subscribe((data: any) => {
      this.users = data;
    });
  }

  togglePanel() {
    this.isRightPanelActive = !this.isRightPanelActive;
  }

  onMobileInput(event: Event) {
    const input = event.target as HTMLInputElement;
    let inputValue = input.value?.toString() || '';
    if (inputValue.length > 10) {
      input.value = inputValue.slice(0, 10);
    } else {
      input.value = inputValue;
    }
  }

  registerSubmit(registerForm: any) {
    let userFound = false;

    for (let user of this.users) {
      if (user.mobileNumber == registerForm.mobile || user.emailId == registerForm.emailId) {
        userFound = true;
        break;
      }
    }

    if (!userFound) {
      localStorage.setItem("mainph",registerForm.mobile);
      this.otpNumber.phoneNumber = "+91" + registerForm.mobile;
      localStorage.setItem("phno", this.otpNumber.phoneNumber);
      this.apiservice.getOtp(this.otpNumber).subscribe((data: any) => { });
      localStorage.setItem("registerform", JSON.stringify(registerForm));
      this.service.unsetCanGo();
      this.service.setotp();
      this.router.navigate(['registerotp'], { relativeTo: this.route });
    } else {
      alert("User already registered with email/phone no. Please sign in");
    }
  }

  loginSubmit(loginForm: any) {
    let userFound = false;

    for (let user of this.users) {
      if (user.mobileNumber === loginForm.mobile1) {
        userFound = true;
        localStorage.setItem("mainph",loginForm.mobile1);
        this.otpNumber.phoneNumber = "+91" + loginForm.mobile1;
        localStorage.setItem("phno", this.otpNumber.phoneNumber);
        this.apiservice.getOtp(this.otpNumber).subscribe((data: any) => { });
        this.service.unsetCanGo();
        this.service.setotp();
        this.router.navigate(['otpValidate'], { relativeTo: this.route });
        break;
      }
    }

    if (!userFound) {
      alert("User not registered. Please sign up");
    }
  }
}
