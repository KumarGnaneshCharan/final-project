import { Component, OnInit } from '@angular/core';
import { EncrDecrService } from '../encr-decr.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { UserapiService } from '../userapi.service';

@Component({
  selector: 'app-register-part2',
  templateUrl: './register-part2.component.html',
  styleUrls: ['./register-part2.component.css']
})
export class RegisterPart2Component implements OnInit {
  registerform: any;
  name: any;
  emailId: any;
  user: any;
  email: any;
  constructor(private service: EncrDecrService, private service1: UserService, private router: Router, private apiservice: UserapiService) {
    this.user = {
      "name": "",
      "mobileNumber": "",
      "emailId": "",
      "password": ""
    };
    this.email = {
      "toEmail": "",
      "subject": "",
      "body": ""
    }
    const storedValue = localStorage.getItem("registerform");
    if (storedValue !== null) {
      this.registerform = JSON.parse(storedValue);
    } else {
      console.error("Restaurant not found in local storage");
    }
    
  }

  ngOnInit() {
    this.name = this.registerform.name;
    this.emailId = this.registerform.emailId;
  }
  show(register2Form: any) {
    this.user.name = this.registerform.name;
    this.user.mobileNumber = this.registerform.mobile;
    this.user.emailId = this.registerform.emailId;
    this.user.password = register2Form.password;
    this.apiservice.registerUser(this.user).subscribe((data: any) => {
      if (data['statusCode'] == 200) {
        alert('Registration Successful!');
      }
    });
    this.email.toEmail = this.registerform.emailId;
    this.email.subject = "Registration Successfull";
    this.email.body = "Thanks for registering on our Dine In Style website. Have a nice day. Thank you"
    this.apiservice.sendEmail(this.email).subscribe((data: any) => { console.log(data) });
    this.service1.unsetCanGoregister();
    this.service1.setUserLogin();
    this.router.navigate(['homepage']);
  }
}
