import { inject } from '@angular/core';
import { CanActivateChildFn } from '@angular/router';
import { UserService } from './user.service';

export const otpAuthGuard: CanActivateChildFn = (childRoute, state) => {
  let service =inject(UserService);
  return service.getOtp();
}
