import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loginStatus:Subject<any>;
  isUserLogged:boolean;
  otpStatus:Subject<any>;
  otp:boolean;
  canGo:boolean;
  register:boolean;
  CanGoregister:boolean;

  constructor() {
    this.isUserLogged=false;
    this.loginStatus=new Subject();
    this.otpStatus=new Subject();
    this.otp=false;
    this.canGo=false;
    this.register=false;
    this.CanGoregister=false;
  }

  setUserLogin(){
    this.isUserLogged=true;
  }
  setUserLogout(){
    this.isUserLogged=false;
  }
  getIsUserLogged():boolean{
    return  this.isUserLogged ;
  }
  getStatusLogin(){
    return this.loginStatus.asObservable();
  }

  setotp(){
    this.otp=true;
  }
  unsetotp(){
    this.otp=false;
  }
  getOtp():boolean{
    return  this.otp ;
  }
  getOtpStatus(){
    return this.otpStatus.asObservable();
  }

  setCanGo(){
    this.canGo=true;
  }
  unsetCanGo(){
    this.canGo=false;
  }
  getCanGo():boolean{
    return  this.canGo ;
  }

  setRegister(){
    this.register=true;
  }
  unsetRegister(){
    this.register=false;
  }
  getRegister(){
    return this.register;
  }

  setCanGoregister(){
    this.CanGoregister=true;
  }
  unsetCanGoregister(){
    this.CanGoregister=false;
  }
  getCanGoregister():boolean{
    return  this.CanGoregister ;
  }
}
