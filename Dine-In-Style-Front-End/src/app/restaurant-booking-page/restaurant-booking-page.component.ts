import { Component, HostListener, OnInit } from '@angular/core';
import { UserapiService } from '../userapi.service';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-restaurant-booking-page',
  templateUrl: './restaurant-booking-page.component.html',
  styleUrls: ['./restaurant-booking-page.component.css']
})
export class RestaurantBookingPageComponent implements OnInit {
  scrolled: boolean = false;
  apikey:string='AIzaSyDRdVcWTCyCnQ5qUf9AtGHrT6LMNmZZejk';
  restaurant:any;
  EmailBody:any;

  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event) {
    if (window.scrollY >= 100) {
      this.scrolled = true;
    } else {
      this.scrolled = false;
    }
  }

  scrollToElement(elementId: string, offset: number = 0) {
    if (elementId === 'home') {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    } else {
      const element = document.getElementById(elementId);
      if (element) {
        const offsetPosition = element.offsetTop - offset;
        window.scrollTo({ top: offsetPosition, behavior: 'smooth' });
      }
    }
  }

  constructor(private apiservice:UserapiService, private router:Router, private sanitizer: DomSanitizer){
    this.restaurant={
      img:"",name:"",address:"",cost:"",cusine:"",rating:"",
        menu:"",ambience:"",img1:"",
        img2:"",img3:"",img4:"",iframe:""      
    }
    this.EmailBody={
      toEmail:"",
      subject:"",
      body:""
    }
  }
  
  ngOnInit() {
    const storedRestaurant=localStorage.getItem("restaurant");
    if (storedRestaurant !== null) {
      this.restaurant = JSON.parse(storedRestaurant);
      this.restaurant.iframe = this.sanitizer.bypassSecurityTrustResourceUrl(this.restaurant.iframe) as SafeResourceUrl;
    } else {
      console.error("Restaurant not found in local storage");
    }
  }

  check(restaurantBookingForm:any){
    this.EmailBody.toEmail=restaurantBookingForm.email;
    this.EmailBody.subject="Thanks for booking.";
    this.EmailBody.body="Here are the details of the dine in booking:\nName"+restaurantBookingForm.firstname+" "+restaurantBookingForm.lastname+", \ntime: "+restaurantBookingForm.time+", \ndate: "+restaurantBookingForm.date;
    this.apiservice.sendEmail(this.EmailBody).subscribe((data:any)=>{
      this.router.navigate(['/homepage']);
      alert("Thanks for booking your table in "+this.restaurant.name+".We will reach you shortly by email. Have a nice day");
    });
    
  }
}
