import { ChangeDetectorRef, Component } from '@angular/core';
import { UserapiService } from '../userapi.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {
  email:any;
  focusFunc(event: Event) {
    const input = event.target as HTMLInputElement;
    input.parentElement?.classList.add('focus');
  }

  blurFunc(event: Event) {
    const input = event.target as HTMLInputElement;
    const parent = input.parentElement;
    if (input.value === '') {
      parent?.classList.remove('focus');
    }
  }
  constructor(private apiservice:UserapiService, private cd: ChangeDetectorRef){
    this.email={
      toEmail:"",
      subject:"",
      body:""
    }
  }
  sendEmail(EmailForm: any) {
    this.email.toEmail = EmailForm.emailId;
    this.email.subject = "Customer Service Mail";
    this.email.body = "Name: " + EmailForm.name + "\nMessage: " + EmailForm.message;
    this.apiservice.recieveEmail(this.email).subscribe((data: any) => {
      EmailForm.emailId = "";
      EmailForm.name = "";
      EmailForm.message = "";
      this.cd.detectChanges();
      alert("Mail sent successfully");
    });
  }
}
