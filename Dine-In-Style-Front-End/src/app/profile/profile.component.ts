import { Component, OnInit } from '@angular/core';
import { UserapiService } from '../userapi.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  dataph:any;
  dataemail:any;
  constructor(private apiservice:UserapiService, private router:Router){}
  ngOnInit() {
    if(localStorage.getItem("mainph") != null){
      this.dataph=localStorage.getItem("mainph"); 
      console.log("ph: "+this.dataph);
    }
    else if(localStorage.getItem("mainemail") != null){
      this.dataemail=localStorage.getItem("maindata"); 
      console.log("email: "+this.dataemail);
    }
  }
  delete(){
    if(this.dataph) this.apiservice.deleteUserbynumber(this.dataph).subscribe((data:any)=>{
      alert("deleted successfully");
      this.router.navigate(["homepage"]);
    });
    else if(this.dataemail) this.apiservice.deleteUserbyemail(this.dataemail).subscribe((data:any)=>{
      alert("deleted successfully");
      this.router.navigate(["homepage"]);
    });
  }
}
