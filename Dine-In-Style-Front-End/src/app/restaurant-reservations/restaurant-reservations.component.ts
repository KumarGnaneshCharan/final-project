import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant-reservations',
  templateUrl: './restaurant-reservations.component.html',
  styleUrls: ['./restaurant-reservations.component.css']
})
export class RestaurantReservationsComponent implements OnInit {
  Restaurants: any;
  constructor() {
    this.Restaurants = [
      { Restraunt: 'Nawaabas', Date: '11/21/2023' },
      { Restraunt: 'kodikurra chittigara', Date: '11/16/2023' },
      { Restraunt: 'kerala kayal', Date: '11/09/2023' },
      { Restraunt: 'Ambaligampa', Date: '11/17/2023' },
      { Restraunt: 'Nawaabas', Date: '11/22/2023' },
      { Restraunt: 'Ambaligampa', Date: '11/26/2023' },
      { Restraunt: 'Nawaabas', Date: '11/22/2023' },
      { Restraunt: 'Nawaabas', Date: '11/22/2023' },
    ];
  }
  ngOnInit() { };
}
