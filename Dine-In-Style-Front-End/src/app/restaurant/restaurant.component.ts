import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {
  restaurants:any;
  rest:any;
  constructor(private router:Router){
    this.restaurants=[
      {img:"assets/restaurants/chitti gaare/outlook.jpg",name:"Kodi Kura Chitti Gara",address:"Gachibowli",cost:"200",cusine:"Raagi muddha, Natukodi pulusu",rating:"4.2",
        menu:"assets/restaurants/chitti gaare/menu.jpg",ambience:"assets/restaurants/chitti gaare/ambience.jpg",img1:"assets/restaurants/chitti gaare/food1.jpg",
        img2:"assets/restaurants/chitti gaare/food2.jpg",img3:"assets/restaurants/chitti gaare/food3.jpg",img4:"assets/restaurants/chitti gaare/food.jpeg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60897.55020340521!2d78.31917606108621!3d17.455076197411103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93c7b31eaaab%3A0xcece91941905d8f5!2sKodi%20Kura%20Chitti%20Gaare%2C%20Kondapur!5e0!3m2!1sen!2sin!4v1699721789667!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/kayal kerala/outlook.jpg",name:"Kayal kerala",address:"Gachibowli",cost:"250",cusine:"Fish thaali, Chicken thaali, Chatti choru",rating:"4.8",
        menu:"assets/restaurants/kayal kerala/menu.jpg",ambience:"assets/restaurants/kayal kerala/ambience.jpg",img1:"assets/restaurants/kayal kerala/food.jpg",
        img2:"assets/restaurants/kayal kerala/food1.png",img3:"assets/restaurants/kayal kerala/food2.jpg",img4:"assets/restaurants/kayal kerala/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2324140185224!2d78.35615917379143!3d17.448586701041926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9331e3d63643%3A0x4c998477a02f04fd!2sKayal%20Kerala%20Restaurant!5e0!3m2!1sen!2sin!4v1699721892949!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/marsala/outlook.jpg",name:"Marsala",address:"Gachibwli",cost:"250",cusine:"Chicken ,Mutton Assorted Mandi",rating:"4.5",
        menu:"assets/restaurants/marsala/menu.jpg",ambience:"assets/restaurants/marsala/ambiencce.jpg",img1:"assets/restaurants/marsala/food.jpeg",
        img2:"assets/restaurants/marsala/food1.jpg",img3:"assets/restaurants/marsala/food2.jpg",img4:"assets/restaurants/marsala/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2274340613085!2d78.3537688737914!3d17.448825201035007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93257765bba7%3A0x8bc6ce2993732da6!2sMarsala%20Food%20Company%2C%20Gachibowli!5e0!3m2!1sen!2sin!4v1699722811555!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/zega/outlook.jpg",name:"Zega",address:"Nanakramguda Rd",cost:"299",cusine:"Massaman lamb curry, Wok tossed lamb",rating:"4.1",
        menu:"assets/restaurants/zega/menu.jpg",ambience:"assets/restaurants/zega/ambience.jpg",img1:"assets/restaurants/zega/food.jpg",
        img2:"assets/restaurants/zega/food1.jpg",img3:"assets/restaurants/zega/food2.jpg",img4:"assets/restaurants/zega/food3.jpeg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.7855736578194!2d78.33435507379073!3d17.4220751018076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb937fa0cace5f%3A0xa7d3484d8c81cb42!2sZega!5e0!3m2!1sen!2sin!4v1699722867306!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/moti mahal deluxe/outlook.jpg",name:"Moti Mahal Delux",address:"Kondapur",cost:"350",cusine:"Singapoori Noodles, Veg kolhapuri",rating:"4.1",
        menu:"assets/restaurants/moti mahal deluxe/menu.jpg",ambience:"assets/restaurants/zega/ambience.jpg",img1:"assets/restaurants/moti mahal deluxe/food.jpg",
        img2:"assets/restaurants/moti mahal deluxe/food1.jpg",img3:"assets/restaurants/moti mahal deluxe/food2.jpg",img4:"assets/restaurants/moti mahal deluxe/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2332680171376!2d78.36092667379145!3d17.448545801043178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb930a0f4db193%3A0x8f1c6ca3655b0088!2sMoti%20Mahal%20Delux%20Hyderabad!5e0!3m2!1sen!2sin!4v1699722922705!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/cloud dining/outlook.jpg",name:"Cloud Dining",address:"HITEC City",cost:"400",cusine:"Cilantro Chicken, Tawa Fish",rating:"4.1",
        menu:"assets/restaurants/cloud dining/menu.jpg",ambience:"assets/restaurants/cloud dining/ambience.jpeg",img1:"assets/restaurants/cloud dining/food.jpg",
        img2:"assets/restaurants/cloud dining/food1.jpg",img3:"assets/restaurants/cloud dining/food2.jpg",img4:"assets/restaurants/cloud dining/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.1072290038132!2d78.3765777737916!3d17.454581100868626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93d6c25c0207%3A0x8b95bafe95913cfe!2sCloud%20Dining!5e0!3m2!1sen!2sin!4v1699723001996!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/Kritunga/outlook.jpg",name:"Kritunga",address:"DLF Road",cost:"299",cusine:"Kheema Biryani, Kritunga Special Mutton",rating:"3.5",
        menu:"assets/restaurants/Kritunga/menu.jpg",ambience:"assets/restaurants/Kritunga/ambience.jpg",img1:"assets/restaurants/Kritunga/food.jpg",
        img2:"assets/restaurants/Kritunga/food1.jpg",img3:"assets/restaurants/Kritunga/food2.jpg",img4:"assets/restaurants/Kritunga/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.284627560784!2d78.35031227379137!3d17.446085901114216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93bd011a4b11%3A0x6fd7dc2974c6e3f6!2sKritunga%20Restaurant!5e0!3m2!1sen!2sin!4v1699723106757!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/mehfil/outlook.jpg",name:"Mehfil",address:"Gachibowli",cost:"150",cusine:"Chicken Biryani, Mix Jumbo pack",rating:"3.9",
        menu:"assets/restaurants/mehfil/menu.jpg",ambience:"assets/restaurants/mehfil/ambience.jpg",img1:"assets/restaurants/mehfil/food.jpg",
        img2:"assets/restaurants/mehfil/food1.jpg",img3:"assets/restaurants/mehfil/food2.jpg",img4:"assets/restaurants/mehfil/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.3548939368534!2d78.3529187737913!3d17.442719901211515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb932001e12bfb%3A0x929e096faef54ceb!2sMehfil%20TakeAway!5e0!3m2!1sen!2sin!4v1699723167355!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/food-by-eatoes/outlook.png",name:"food-by-eatoes",address:"Prashanth Nagar Colony",cost:"100",cusine:"Idly, Parota",rating:"1.0",
        menu:"assets/restaurants/food-by-eatoes/menu.jpg",ambience:"assets/restaurants/food-by-eatoes/ambience.jpg",img1:"assets/restaurants/food-by-eatoes/food.jpg",
        img2:"assets/restaurants/food-by-eatoes/food1.jpg",img3:"assets/restaurants/food-by-eatoes/food2.jpg",img4:"assets/restaurants/food-by-eatoes/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3805.9540115713176!2d78.3599367737918!3d17.461915100656547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb937e3ecbc727%3A0x8f5dd90163640d03!2sfood%20by%20eatoes!5e0!3m2!1sen!2sin!4v1699723207299!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/Jai's kitchen/outlook.jpg",name:"Jai's Kitchen",address:"Kondapur",cost:"250",cusine:"Chicken Tikka",rating:"4.0",
        menu:"assets/restaurants/Jai's kitchen/menu.jpg",ambience:"assets/restaurants/Jai's kitchen/ambience.jpg",img1:"assets/restaurants/Jai's kitchen/food.jpeg",
        img2:"assets/restaurants/Jai's kitchen/food1.jpeg",img3:"assets/restaurants/Jai's kitchen/food2.jpg",img4:"assets/restaurants/Jai's kitchen/food3.jpeg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.0045693536917!2d78.34863927379176!3d17.459495400726578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9322d72a23df%3A0x2bd3883412ba7ac1!2zSkFJ4oCZUyBLSVRDSEVO!5e0!3m2!1sen!2sin!4v1699723251413!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/mamagoto/outlook.jpg",name:"Mamagoto",address:"Western Pearl",cost:"400",cusine:"Pan-Asian foods",rating:"4.5",
        menu:"assets/restaurants/mamagoto/menu.jpg",ambience:"assets/restaurants/mamagoto/ambience.jpeg",img1:"assets/restaurants/mamagoto/food.jpg",
        img2:"assets/restaurants/mamagoto/food1.jpg",img3:"assets/restaurants/mamagoto/food2.jpg",img4:"assets/restaurants/mamagoto/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.02828615084!2d78.37050217379165!3d17.45836020075939!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93d026679481%3A0xa6647645dbdc4597!2sMamagoto!5e0!3m2!1sen!2sin!4v1699723291242!5m2!1sen!2sin"
      },
      {img:"assets/restaurants/Nawaab's/outlook.jpg",name:"Nawaab's Restaurant",address:"Gachibowli",cost:"310",cusine:"Arabian Mandi, Khabsha",rating:"4.3",
        menu:"assets/restaurants/Nawaab's/menu.jpg",ambience:"assets/restaurants/Nawaab's/ambience.jpg",img1:"assets/restaurants/Nawaab's/food.jpg",
        img2:"assets/restaurants/Nawaab's/food1.jpg",img3:"assets/restaurants/Nawaab's/food2.jpg",img4:"assets/restaurants/Nawaab's/food3.jpg",
        iframe:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.2233331162047!2d78.36147277379143!3d17.449021601029482!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb93ab7d8bcc7f%3A0xf8de83173cd01211!2sThe%20Nawaab&#39;s%20Restaurant!5e0!3m2!1sen!2sin!4v1699723327333!5m2!1sen!2sin"
      }
    ]
    this.rest={
      img:"",name:"",address:"",cost:"",cusine:"",rating:"",
        menu:"",ambience:"",img1:"",
        img2:"",img3:"",img4:"",iframe:""      
    }
  }
  ngOnInit() {}
  getRatingBackgroundColor(rating: number) {
    if(rating>=4.3){
      return 'darkgreen';
    }else if (rating >= 3.8) {
      return 'green-bg';
    } else if (rating >= 3) {
      return 'yellowish-orange-bg';
    } else {
      return 'red-bg';
    }
  }

  navigate(restaurant:any){
    localStorage.setItem("restaurant",JSON.stringify(restaurant));
    this.router.navigate(['restaurantbooking']);
  }
}
