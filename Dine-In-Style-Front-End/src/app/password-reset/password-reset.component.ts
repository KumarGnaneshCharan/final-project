import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserapiService } from '../userapi.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent {
  user:any;
  mobile:any;
  constructor(private router:Router,private apiservice:UserapiService){
    this.mobile=localStorage.getItem("mobile");
    this.apiservice.getUserByNumber(this.mobile).subscribe((data:any)=>{
      this.user=data;
    });
  }

  resetPassword(resetForm:any){
    console.log(resetForm);
    this.user.password=resetForm.password;
    this.apiservice.updateUser(this.user).subscribe((data:any)=>{
      alert('Your password has been updated successfully');
      localStorage.removeItem("phno");
      localStorage.removeItem("mobile");
    });
    this.router.navigate(['loginwithgmail']);
  }
}
