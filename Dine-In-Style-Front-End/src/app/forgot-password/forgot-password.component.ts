import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { UserapiService } from '../userapi.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  otpNumber: any;
  users: any;
  constructor(private router: Router, private route: ActivatedRoute, private service: UserService, private apiservice: UserapiService) {
    this.otpNumber = { "phoneNumber": "" };
  }

  ngOnInit() {
    this.apiservice.getAllUsers().subscribe((data: any) => {
      this.users = data;
    });
  }

  onMobileInput(event: Event) {
    const input = event.target as HTMLInputElement;
    let inputValue = input.value?.toString() || '';
    if (inputValue.length > 10) {
      input.value = inputValue.slice(0, 10);
    } else {
      input.value = inputValue;
    }
  }

  resetPassword(resetform: any) {
    let userFound = false;

    for (let user of this.users) {
      if (user.mobileNumber == resetform.mobile1) {
        userFound = true;
        this.otpNumber.phoneNumber = "+91" + resetform.mobile1;
        localStorage.setItem("phno", this.otpNumber.phoneNumber);
        localStorage.setItem("mobile",resetform.mobile1)
        this.apiservice.getOtp(this.otpNumber).subscribe((data: any) => { });
        this.service.unsetCanGo();
        this.service.setotp();
        this.router.navigate(['resetOtp'], { relativeTo: this.route });
        break;
      }
    }

    if (!userFound) {
      alert("User not registered. Please sign up");
    }
  }
}
