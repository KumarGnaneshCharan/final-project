package com.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {
    @Autowired
    private JavaMailSender mailSender;

    public void sendEmail(EmailRequest emailrequest) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("dineinstyle.reservations@gmail.com");
        message.setTo(emailrequest.getToEmail());
        message.setText(emailrequest.getBody());
        message.setSubject(emailrequest.getSubject());
        mailSender.send(message);
        System.out.println("Email sent");
    }
    public void recieveEmail(EmailRequest emailrequest) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(emailrequest.getToEmail());
        message.setTo("dineinstyle.reservations@gmail.com");
        message.setText(emailrequest.getBody());
        message.setSubject(emailrequest.getSubject());
        mailSender.send(message);
        System.out.println("Email recieve");
    }
}
