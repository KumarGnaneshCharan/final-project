package com.sms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtpValidationRequest {
	private String phoneNumber;
	private String otpNumber;
	public OtpValidationRequest() {
		super();
	}
	public OtpValidationRequest(@JsonProperty("phoneNumber") String phoneNumber, @JsonProperty("otpNumber") String otpNumber) {
		this.phoneNumber = phoneNumber;
		this.otpNumber = otpNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getOtpNumber() {
		return otpNumber;
	}
	public void setOtpNumber(String otpNumber) {
		this.otpNumber = otpNumber;
	}
	
}