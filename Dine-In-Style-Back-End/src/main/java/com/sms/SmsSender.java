package com.sms;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface SmsSender {

    void sendSms(SmsRequest smsRequest);
    ResponseEntity<String> otpValidate(OtpValidationRequest otp);
}
