package com.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;

@org.springframework.stereotype.Service
public class Service {

    private final SmsSender smsSender;

    @Autowired
    public Service(@Qualifier("twilio") TwilioSmsSender smsSender) {
        this.smsSender = smsSender;
    }
    
    public void sendSms(SmsRequest smsRequest) {
        smsSender.sendSms(smsRequest);
    }
    
    public ResponseEntity<String> otpValidate(OtpValidationRequest otp){
    	return smsSender.otpValidate(otp);
    }
}
