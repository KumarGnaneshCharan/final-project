package com.sms;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

import java.text.DecimalFormat;
import java.util.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("twilio")
public class TwilioSmsSender implements SmsSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwilioSmsSender.class);

    private final TwilioConfiguration twilioConfiguration;
    
    Map<String, String> otpMap = new HashMap<>();

    @Autowired
    public TwilioSmsSender(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
    }

    public void sendSms(SmsRequest smsRequest) {
        if (isPhoneNumberValid(smsRequest.getPhoneNumber())) {
            PhoneNumber to = new PhoneNumber(smsRequest.getPhoneNumber());
            PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
            String otp = generateOTP();
			String otpMessage = "Dear Customer , Your OTP for logging is  " + otp + ". Thank You.";
            MessageCreator creator = Message.creator(to, from, otpMessage);
            creator.create();
            otpMap.put(smsRequest.getPhoneNumber(), otp);
            LOGGER.info("Send sms {}", smsRequest);
        } else {
            throw new IllegalArgumentException(
                    "Phone number [" + smsRequest.getPhoneNumber() + "] is not a valid number"
            );
        }
    }
    
    @Override
    public ResponseEntity<String> otpValidate(OtpValidationRequest otp) {
    	String phoneNumber = otp.getPhoneNumber();
        String enteredOtp = otp.getOtpNumber();
        String responseJson ="";

        if (otpMap.containsKey(phoneNumber)) {
            String storedOtp = otpMap.get(phoneNumber);

            if (enteredOtp.equals(storedOtp)) {
                otpMap.remove(phoneNumber); // Remove the OTP after successful validation.
                responseJson = "{\"status\": \"success\"}";
            }
        }
        return new ResponseEntity<>(responseJson, HttpStatus.OK);
        
	}
    
    private boolean isPhoneNumberValid(String phoneNumber) {
        return true;
    }
    
    private static String generateOTP() {
        return new DecimalFormat("0000")
                .format(new Random().nextInt(9999));
    }
}
