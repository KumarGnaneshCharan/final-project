package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sms.OtpValidationRequest;
import com.sms.Service;
import com.sms.SmsRequest;

@RestController
@RequestMapping("otp")
public class SmsController {

    private final Service service;

    @Autowired
    public SmsController(Service service) {
        this.service = service;
    }

    @PostMapping("send")
    public void sendSms(@RequestBody SmsRequest smsRequest) {
        service.sendSms(smsRequest);
    }
    
    @PostMapping("verify")
    public ResponseEntity<String> sendSms(@RequestBody OtpValidationRequest otp) {
        return service.otpValidate(otp);
    }
}
