package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.email.EmailRequest;
import com.email.EmailSenderService;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private EmailSenderService senderService;

    @PostMapping("/send")
    public void sendEmail(@RequestBody EmailRequest emailRequest) throws MessagingException {
    	senderService.sendEmail(emailRequest);
    }
    @PostMapping("/recieve")
    public void recieveEmail(@RequestBody EmailRequest emailRequest) throws MessagingException {
    	senderService.recieveEmail(emailRequest);
    }
}
