package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.Users;

@RestController
public class UserController {
	@Autowired
	UserDAO userDAO;
	@GetMapping("getAllUsers")
	public List <Users>getAllUsers(){
		return userDAO.getAllUsers();
	}
	@GetMapping("getUserByMobileNumber/{number}")
	public Users getUserByMobileNumber(@PathVariable("number") long number){
		return userDAO.getUserByMobileNumber(number);
	}
	@GetMapping("getUserByEmailId/{emailId}")
	public Users getUserByEamailId(@PathVariable("emailId") String emailId){
		return userDAO.getUserByemailId(emailId);
	}
	@PostMapping("registerUser")
	public Users registerUser(@RequestBody Users user){
		return userDAO.registerUser(user);
	}
	@PutMapping("updateUser")
	public Users updateUser(@RequestBody Users user){
		return userDAO.updateUser(user);
	}
	@DeleteMapping("deleteUserByNumber/{number}")
	public String deleteUserByMobileNumber(@PathVariable("number") long number){
		userDAO.deleteUserByMobileNumber(number);
		return "user deleted ......";
	}
	@DeleteMapping("deleteUserByEmail/{emailId}")
	public String deleteUserByEmailId(@PathVariable("emailId") String emailId){
		userDAO.deleteUserByemailId(emailId);
		return "user deleted ......";
	}
}
