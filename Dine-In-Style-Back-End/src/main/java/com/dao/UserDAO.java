package com.dao;

import java.util.List;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Users;

@Service
public class UserDAO {
   @Autowired
   UserRepository userRepo;
   public List<Users> getAllUsers() {
		return userRepo.findAll();
	}
	public Users registerUser(Users user) {
		return userRepo.save(user);
	}
	public Users updateUser(Users user) {
		return userRepo.save(user);
	}
	public void deleteUserByMobileNumber(long number) {
		userRepo.deleteUserByMobileNumber(number);
	}
	public Users getUserByMobileNumber(long number){
		return userRepo.findByMobileNumber(number);
	}
	public Users getUserByemailId(String emailId){
		return userRepo.findByemailId(emailId);
	}
	public void deleteUserByemailId(String emailId) {
		userRepo.deleteUserByemailId(emailId);
	}
}
