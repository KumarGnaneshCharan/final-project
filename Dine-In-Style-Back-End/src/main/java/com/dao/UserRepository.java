package com.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.model.Users;

public interface UserRepository extends JpaRepository<Users,Integer> {
	@Query("from Users where mobileNumber = :mobileNumber")
	Users findByMobileNumber(@Param("mobileNumber") long mobileNumber);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Users WHERE mobileNumber = :mobileNumber")
	void deleteUserByMobileNumber(@Param("mobileNumber") long mobileNumber);
	
	@Query("from Users where EmailId = :emailId")
	Users findByemailId(@Param("emailId") String emailId);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Users WHERE EmailId = :emailId")
	void deleteUserByemailId(@Param("emailId") String emailId);
}
